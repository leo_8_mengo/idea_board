from django.forms import ModelForm
from .models import Ideas

class IdeaForm(ModelForm):
    class Meta:
        model = Ideas
        fields = ['dono','tittle','idea']