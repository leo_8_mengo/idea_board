from django.shortcuts import render
from django.views.generic.list import ListView
from django import template
from django.views import generic
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from ideas.models import Ideas
from .forms import IdeaForm

class IdeasView(ListView):

    model = Ideas
    template_name = "home/home.html"
    register = template.Library()

    def get_context_data(self, **kwargs):
        if str(self.request.user) != 'AnonymousUser':
            context = super().get_context_data(**kwargs)        
            context['ideas'] = Ideas.objects.filter(dono=self.request.user)
            return context
            


@method_decorator(login_required(login_url='Ideas_Home'), name='dispatch')
class NewIdea(generic.CreateView):
    model = Ideas
    fields = ['tittle','idea']
    success_url = reverse_lazy('Ideas_Home')
    template_name = 'newidea/newidea.html'

    def form_valid(self, form):
        form.instance.dono = self.request.user
        return super().form_valid(form)


class DeleteIdea(generic.DeleteView):
    model = Ideas
    success_url = reverse_lazy('Ideas_Home')

class UpdateIdea(generic.UpdateView): 
    model = Ideas   
    fields = ['tittle','idea']
    template_name = 'updateidea/updateidea.html'
    success_url = reverse_lazy('Ideas_Home')