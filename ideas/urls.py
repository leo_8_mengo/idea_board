from django.urls import path
from ideas.views import IdeasView, NewIdea, DeleteIdea, UpdateIdea

urlpatterns = [
    path('', IdeasView.as_view(), name='Ideas_Home'),
    path('newidea/', NewIdea.as_view(), name='new_idea'),
    path('deleteidea/<int:pk>/', DeleteIdea.as_view(), name='delete_idea'),
    path('updateidea/<int:pk>/', UpdateIdea.as_view(), name='update_idea'),
]