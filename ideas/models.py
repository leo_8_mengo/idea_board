from django.db import models
from django.contrib.auth.models import User

class Ideas(models.Model):
    dono = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    tittle = models.CharField(max_length=150)
    idea = models.TextField(blank=True, null=True)
    
    def __str__(self):
        return self.tittle    
